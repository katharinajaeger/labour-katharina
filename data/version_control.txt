users:
01 - 
03 - bugfix: contain also users with labor recordings 0% sad segments
04 - limitation to max 10% sad segments per recording
05 - same as 03
06 - same as 04
07 - delete users with features after delivery


recordings:
01 - old labor definition
02 - new labor definition (leave one day out)
03 - bugfix: contain also users with labor recordings 0% sad segments
04 - limitation to max 10% sad segments per recording
05 - same as 03, but no labor column
06 - same as 04
07 - delete users with features after delivery

contractions:
01 - old labor definition
02 - new labor definition (leave one day out)
03 - bugfix: contain also users with labor recordings 0% sad segments
04 - limitation to max 10% sad segments per recording
05 - same as 03, but no labor column
06 - shorter than 150s
07 - delete users with features after delivery

features:
06 - computed with contractions shorter than 150s
07 - no labor definition but days to delivery + delete users with features after delivery




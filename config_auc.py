###############definitions###########

run_train = 'train'

run_test = 'test'

label_labor = 'labor'

label_TTD = 'TTD'

data_paths_labor = {'train': {'recordings': './data/data/train/labor/recordings.pkl',
                             'contractions': './data/data/train/labor/contractions.pkl'},
             'test': {'recordings': './data/data/test/labor/recordings.pkl',
                             'contractions': './data/data/test/labor/contractions.pkl'}}

data_paths_TTD = {'train': {'recordings': './data/data/train/TTD/recordings.pkl',
                             'contractions': './data/data/train/TTD/contractions.pkl'},
             'test': {'recordings': './data/data/test/TTD/recordings.pkl',
                             'contractions': './data/data/test/TTD/contractions.pkl'}} 

##################config#############

run = run_train 

label = label_TTD

data_paths = data_paths_TTD





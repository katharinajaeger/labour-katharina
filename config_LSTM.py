###################definitions##########
features_all = ['count', 'duration_median', 'duration_iqr', 'duration_rmssd',
                                     'auc_median', 'auc_iqr', 'auc_rmssd', 'variability', 'regularity_0', 'regularity_1',
                                     'regularity_2' , 'regularity_e', 'rmssd', 'gestational_age']

features_GA = ['gestational_age']

features_noGA = ['count', 'duration_median', 'duration_iqr', 'duration_rmssd',
                                     'auc_median', 'auc_iqr', 'auc_rmssd', 'variability', 'regularity_0', 'regularity_1',
                                     'regularity_2' , 'regularity_e', 'rmssd']

label_TTD1 = {'type': 'time to delivery 1 week',
             'num_classes': 5}

label_TTD2 = {'type': 'time to delivery 2 week',
             'num_classes': 3}

KFold = {'type': 'KFold',
              'splits': 5,
         'balancing': 'cweights'}

SKFold = {'type': 'SKFold',
          'splits': 5,
         'balancing': 'cweights'}

time_binning = {'binning': True,
                'bin_size': 2, # in days
                'add_info': './data/features/train/TTD/2020-01-22_17-08-28/users.pkl'}

stringed_features1 =  {'binning': False,
               'version': 1,
               'length': 550}

stringed_features2 =  {'binning': False,
               'version': 2,
               'length': 50}

stringed_features3 =  {'binning': False,
               'version': 3,
               'length': 50,
                'synthesize': True}

#####################config#############
features = {'names': features_all,
            'path': './data/features/train/TTD/2020-01-22_17-08-28/edge/features.pkl',
           'analysis_path': './data/features/train/TTD/2020-01-22_17-08-28/noEdge/features.pkl'}

label = label_TTD1

time_dimension = stringed_features3

params = {'epochs': 500, 
          'batch_size': 32}

validation = SKFold

# baue verschiedene sets an parameter und setze label dann im config file auf die entsprechende auswahl

####label####
label_labor = {'type': 'labor', # GA at birth...
         'additional_info': './data/features/train/labor/2020-01-13_13-45-33/recordings.pkl' #path to labor info
        }

label_TTD1 = {'type': 'time to delivery 1 week',
             'num_classes': 19}

label_TTD2 = {'type': 'time to delivery 2 week',
             'num_classes': 19}

label_GA = {'type': 'GA'}

####validation####
cweights = {'splits': 5,
         'balancing': 'cweights'}
smote = {'splits': 5,
         'balancing': 'smo'}
rus = {'splits': 5,
         'balancing': 'rus'}


####features####
features_all = ['count', 'duration_median', 'duration_iqr', 'duration_rmssd',
                                     'auc_median', 'auc_iqr', 'auc_rmssd', 'variability', 'regularity_0', 'regularity_1',
                                     'regularity_2' , 'regularity_e', 'rmssd', 'gestational_age']

features_GA = ['gestational_age']

features_noGA = ['count', 'duration_median', 'duration_iqr', 'duration_rmssd',
                                     'auc_median', 'auc_iqr', 'auc_rmssd', 'variability', 'regularity_0', 'regularity_1',
                                     'regularity_2' , 'regularity_e', 'rmssd']
####paths####
labor_paths = {'path': './data/features/train/labor/2020-01-13_13-45-33/edge/features.pkl',
           'analysis_path': './data/features/train/labor/2020-01-13_13-45-33/noEdge/features.pkl'}

TTD_paths = {'path': './data/features/train/TTD/2020-01-22_17-08-28/edge/features.pkl',
         'analysis_path': './data/features/train/TTD/2020-01-22_17-08-28/noEdge/features.pkl'}


#---------------------------------------------CONFIG---------------------------------------------------------------------------#

features = {'names': features_all,
            'paths': labor_paths}
 

label = label_labor


validation = cweights












           
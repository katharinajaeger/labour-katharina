import numpy as np
import pandas as pd
import random

def balance_binary(label_name, features, undersampled_label):
    if undersampled_label == 1:
        oversampled_label = 0
    else:
        oversampled_label = 1

    sample_size = len(features[features[label_name] == undersampled_label])
    sample_indices = random.choices(features[features[label_name] == oversampled_label].index.values, k=sample_size)

    # undersample classes
    features_0 = features.loc[sample_indices]
    features_1 = features[features[label_name] == undersampled_label]
    features_underS = pd.concat([features_0, features_1])

    return features_underS
import numpy as np
import pandas as pd
from . import regression
#import regression


def test_features(regression_slopes, n_sim):
    feature_names = regression_slopes.columns.values
    # store in df
    t_values = pd.DataFrame(columns=['t', 't_increase', 't_decrease', 'feature'])

    for feature in feature_names:
        if feature == 'user_id':
            continue
        t, t_increase, t_decrease = do_permutation(regression_slopes[feature], n_sim)
        #t_values = t_values.set_index(['feature'])
        t_values = t_values.append({'feature': feature, 't': t, 't_increase': t_increase, 't_decrease': t_decrease}, ignore_index=True)
        ...
    return t_values



def do_permutation(slopes, n_sim):
    slopes = slopes.dropna().values.astype(float)
    standard = np.mean(slopes)
    n = len(slopes)
    perm_means = np.zeros(n_sim)

    for i in range(n_sim):
        a = np.random.rand(n)
        mask = [-1 if x < 0.5 else 1 for x in a]
        perm_means[i] = np.mean([a * b for a, b in zip(mask, np.abs(slopes))])


    t_increase = len(perm_means[perm_means >= standard]) / n_sim
    t_decrease = len(perm_means[perm_means <= standard]) / n_sim
    t = (len(perm_means[perm_means >= np.abs(standard)]) + len(perm_means[perm_means <= -np.abs(standard)])) / n_sim
    return t, t_increase, t_decrease

# features = pd.read_pickle('./../../data/04/features-04.pkl')
# feature_names = feature_names = ['count', 'duration_median', 'duration_iqr', 'duration_rmssd',
#                                      'auc_median', 'auc_iqr', 'auc_rmssd', 'variability', 'regularity_0', 'regularity_1',
#                                      'regularity_2' , 'regularity_e', 'rmssd']
# r = regression.fit(features, feature_names)
# t = test_features((r))


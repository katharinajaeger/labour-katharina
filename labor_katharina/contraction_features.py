#extracts number of contractions in windows for given window size

import numpy as np
import pandas as pd
import statistics as stat

def extract(recordings, contractions, window_size, corner=False):
    features = pd.DataFrame(columns=['count', 'duration_min', 'duration_max', 'duration_mean', 'duration_median',
                                             'duration_quantile_1', 'duration_quantile_3', 'duration_sd', 'duration_rmssd', 'duration_maxmin',
                                            'timebetween_min', 'timebetween_max', 'timebetween_mean', 'timebetween_median',
                                             'timebetween_quantile_1', 'timebetween_quantile_3', 'timebetween_sd',
                                     'timebetween_maxmin', 'timebetween_rmssd',
                                     'session_id', 'labor', 'start_window'])


    # for each recording in recordings
    # for each window between start rec and end rec
    #  look how many contractions in list of contractions

    for index, rowR in recordings.iterrows():
        num_wins = int(np.floor(rowR['duration'].total_seconds() / window_size))
        cons = contractions[contractions['session_id'] == index]
        for win in range(0, num_wins):
            count = 0
            durations = []
            peaks = []
            start_window = rowR['created_at'] + pd.Timedelta(window_size * win, 's')
            for _, rowC in cons.iterrows():
                peak = rowC['started_at'] + pd.Timedelta(int(rowC['delta_onset_to_peak']), 's')
                if (peak - start_window < pd.Timedelta(window_size, 's') and peak - start_window >= pd.Timedelta(0, 's')):
                    count = count + 1
                    durations.append(rowC['duration'].total_seconds())
                    # create subset with peak time
                    peaks.append(peak)
            peaks.sort()
            peak_diffs = [(t - s).total_seconds() for s, t in zip(peaks, peaks[1:])]

            #feature computation  some features need minimum number of contractions in window  default values for corner cases to avoid NaNs
            # corner cases according to \Users\jaege\OneDrive\Documents\Bloomlife\Data\Customer\contraction_features_CornerCases.xlsx
            #default values are weighted according to number of contractions in window (more information if contraction)
            if(len(durations) == 0):
                duration_min = [35 if corner else None][0]
                duration_max = [35 if corner else None][0]
                duration_mean = [35 if corner else None][0]
                duration_median = [35 if corner else None][0]
                duration_quantile_1 = [35 if corner else None][0]
                duration_quantile_3 = [35 if corner else None][0]
                duration_sd = [63*1.5 if corner else None][0]
                duration_maxmin = [125*1.5 if corner else None][0]
                duration_rmssd = [125*2 if corner else None][0]

                timebetween_min = [window_size*1.5 if corner else None][0]
                timebetween_max = [window_size*1.5 if corner else None][0]
                timebetween_mean = [window_size*1.5 if corner else None][0]
                timebetween_median = [window_size*1.5 if corner else None][0]
                timebetween_quantile_1 = [window_size*1.5 if corner else None][0]
                timebetween_quantile_3 = [window_size*1.5 if corner else None][0]
                timebetween_sd = [((window_size-80)/2)*2 if corner else None][0]#((window_size-80)/2)*2
                timebetween_maxmin = [(window_size-80)*2 if corner else None][0]#(window_size-80)*2
                timebetween_rmssd = [(window_size-120)*2.5 if corner else None][0]#(window_size-120)*2.5

            elif(len(durations) == 1):
                duration_min = np.min(durations)
                duration_max = np.max(durations)
                duration_mean = np.mean(durations)
                duration_median = stat.median(durations)
                duration_quantile_1 = np.quantile(durations, 0.25)
                duration_quantile_3 = np.quantile(durations, 0.75)
                duration_sd = [63 if corner else None][0]#63
                duration_maxmin = [125 if corner else None][0]#125
                duration_rmssd = [125*1.5 if corner else None][0]#125 * 1.5

                timebetween_min = [window_size if corner else None][0]#window_size
                timebetween_max = [window_size if corner else None][0]#window_size
                timebetween_mean = [window_size if corner else None][0]#window_size
                timebetween_median = [window_size if corner else None][0]#window_size
                timebetween_quantile_1 = [window_size if corner else None][0]#window_size
                timebetween_quantile_3 = [window_size if corner else None][0]#window_size
                timebetween_sd = [((window_size - 80) / 2) * 1.5 if corner else None][0]#((window_size - 80) / 2) * 1.5
                timebetween_maxmin = [(window_size - 80) * 1.5 if corner else None][0]#(window_size - 80) * 1.5
                timebetween_rmssd = [(window_size - 120) * 2 if corner else None][0]#(window_size - 120) * 2

            elif(len(durations) == 2):
                duration_min = np.min(durations)
                duration_max = np.max(durations)
                duration_mean = np.mean(durations)
                duration_median = stat.median(durations)
                duration_quantile_1 = np.quantile(durations, 0.25)
                duration_quantile_3 = np.quantile(durations, 0.75)
                duration_sd = np.std(durations)
                duration_maxmin = duration_max-duration_min
                duration_rmssd = [125 if corner else None][0]#125

                timebetween_min = np.min(peak_diffs)
                timebetween_max = np.max(peak_diffs)
                timebetween_mean = np.mean(peak_diffs)
                timebetween_median = stat.median(peak_diffs)
                timebetween_quantile_1 = np.quantile(peak_diffs, 0.25)
                timebetween_quantile_3 = np.quantile(peak_diffs, 0.75)
                timebetween_sd = [((window_size - 80) / 2) if corner else None][0]#((window_size - 80) / 2)
                timebetween_maxmin = [(window_size - 80) if corner else None][0]#(window_size - 80)
                timebetween_rmssd = [(window_size - 120)*1.5 if corner else None][0]#(window_size - 120) * 1.5

            elif(len(durations) == 3):
                duration_min = np.min(durations)
                duration_max = np.max(durations)
                duration_mean = np.mean(durations)
                duration_median = stat.median(durations)
                duration_quantile_1 = np.quantile(durations, 0.25)
                duration_quantile_3 = np.quantile(durations, 0.75)
                duration_sd = np.std(durations)
                duration_maxmin = duration_max - duration_min
                duration_rmssd = np.sqrt(np.mean([np.square(t - s) for s, t in zip(durations, durations[1:])]))

                timebetween_min = np.min(peak_diffs)
                timebetween_max = np.max(peak_diffs)
                timebetween_mean = np.mean(peak_diffs)
                timebetween_median = stat.median(peak_diffs)
                timebetween_quantile_1 = np.quantile(peak_diffs, 0.25)
                timebetween_quantile_3 = np.quantile(peak_diffs, 0.75)
                timebetween_sd = np.std(peak_diffs)
                timebetween_maxmin = timebetween_max-timebetween_min
                timebetween_rmssd = [(window_size - 120) if corner else None][0]#(window_size - 120)

            else:
                duration_min = np.min(durations)
                duration_max = np.max(durations)
                duration_mean = np.mean(durations)
                duration_median = stat.median(durations)
                duration_quantile_1 = np.quantile(durations, 0.25)
                duration_quantile_3 = np.quantile(durations, 0.75)
                duration_sd = np.std(durations)
                duration_maxmin = duration_max - duration_min
                duration_rmssd = np.sqrt(np.mean([np.square(t - s) for s, t in zip(durations, durations[1:])]))

                timebetween_min = np.min(peak_diffs)
                timebetween_max = np.max(peak_diffs)
                timebetween_mean = np.mean(peak_diffs)
                timebetween_median = stat.median(peak_diffs)
                timebetween_quantile_1 = np.quantile(peak_diffs, 0.25)
                timebetween_quantile_3 = np.quantile(peak_diffs, 0.75)
                timebetween_sd = np.std(peak_diffs)
                timebetween_maxmin = timebetween_max - timebetween_min
                timebetween_rmssd = np.sqrt(np.mean([np.square(t - s) for s, t in zip(peak_diffs, peak_diffs[1:])]))


            features = features.append(
                {'count': count, 'duration_min': duration_min, 'duration_max': duration_max, 'duration_mean': duration_mean,
                 'duration_median':duration_median, 'duration_quantile_1': duration_quantile_1, 'duration_quantile_3': duration_quantile_3,
                 'duration_sd': duration_sd, 'duration_maxmin': duration_maxmin, 'duration_rmssd': duration_rmssd,
                 'timebetween_min': timebetween_min, 'timebetween_max': timebetween_max, 'timebetween_mean': timebetween_mean,
                 'timebetween_median': timebetween_median, 'timebetween_quantile_1': timebetween_quantile_1,
                 'timebetween_quantile_3': timebetween_quantile_3, 'timebetween_sd': timebetween_sd, 'timebetween_maxmin': timebetween_maxmin,
                 'timebetween_rmssd': timebetween_rmssd,
                 'session_id': rowR['id'], 'labor': rowR['labor'], 'start_window': start_window},
                ignore_index=True)

    features.sort_values('start_window')
    features = features.reset_index(drop=True)
    return features


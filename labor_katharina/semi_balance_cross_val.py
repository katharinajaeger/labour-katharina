import numpy as np
import random

def split(n_splits, X, y):
    for i in range(n_splits):
        yield run(X,y)

def run(X, y):
    # compute class proportions
    # loop for n splits
    # get balanced train set with 0.8% of minimum class size
    # take rest of data and select randomly samples from it depending on initial proportion

    # -------train-------
    # compute class proportions from original distribution
    props = [len([i for i in y if i == c]) / len(y) for c in set(y)]
    size_train = int(np.ceil(min(props) * len(y) * 0.8))  # only 0.8 of smallest class are used; rest for testing

    # sample randomly from the classes and store indices
    indices_train = []
    [indices_train.extend(random.sample([i for i, e in enumerate(y) if e == c], size_train)) for c in set(y)]

    #X_train = [X[i] for i in indices_train]
    #y_train = [y[i] for i in indices_train]

    # ---------test-------
    # remove train data from potential test data
    y_tmp = [e for i, e in enumerate(y) if not i in indices_train]
    y_tmp_i = [i for i, e in enumerate(y) if not i in indices_train]

    total_abs = []
    [total_abs.append(np.floor(len([y_tmp_i[i] for i, e in enumerate(y_tmp) if e == c]) / props[c])) for c in set(y)]

    total = int(min(total_abs))
    target_sizes = [int(np.ceil(x * total)) for x in props]

    indices_test = []
    [indices_test.extend(random.sample([y_tmp_i[i] for i, e in enumerate(y_tmp) if e == c], int(target_sizes[c]))) for c
     in set(y)]

    #X_test = [X[i] for i in indices_test]
    #y_test = [y[i] for i in indices_test]

    #return X_train, y_train, X_test, y_test
    return indices_train, indices_test

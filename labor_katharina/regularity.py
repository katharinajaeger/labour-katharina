# compute regularity of contractions
# inspired by Labarre 2008

# return entropy as a measure of regularity (maybe not reliable)
# and probability of 0,1 and 2 contractions in a specific window

import numpy as np
import pandas as pd
import scipy.stats as scp

def get(peaks, peaks_diff, window_size, start_window):
    bin_size = np.mean(peaks_diff)
    num_bins = int(np.floor(window_size/bin_size))
    density_histogram = np.zeros((len(peaks)))

    for bin in range(0, num_bins):
        #check how many contractions are in bin
        start_bin = start_window + pd.Timedelta(bin_size*bin, 's')
        event_count = 0
        for peak in peaks:
            if (peak - start_bin < pd.Timedelta(bin_size, 's') and peak - start_bin >= pd.Timedelta(0, 's')):
                event_count = event_count+1
        density_histogram[event_count] = density_histogram[event_count]+1
    density_histogram = density_histogram/num_bins
    #hist_bars = {i: density_histogram[i] for i in range(len(density_histogram)) if density_histogram[i] != 0} # convert to list, only keep non zero bars

    entropy = scp.entropy(density_histogram)

    if(len(density_histogram)==1):
        return density_histogram[0], 0, 0, entropy
    elif(len(density_histogram)==2):
        return density_histogram[0], density_histogram[1], 0, entropy
    else:
        return density_histogram[0], density_histogram[1], density_histogram[2], entropy

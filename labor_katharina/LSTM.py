import numpy as np

def rearrange_features(features_orig, users, bin_size, feature_names):
    time_length = int(12 * 7 / bin_size)

    uids = list(set(features_orig['user_id']))
    features = np.zeros((len(uids), time_length + 1, len(feature_names)))
    labels = np.zeros((len(uids)))

    for i_usr, uid in enumerate(uids):
        print('Processing user', i_usr, '/', len(uids))
        ttd = []

        # summarize features before week 30 to one timestep
        f = features_orig[features_orig['user_id'] == uid]
        f = f.sort_values(by=['start_window'])

        # compute median from all features below 30 weeks
        # f_0 = [a<30 for a in f['gestational_age']]
        f_0 = f[f['gestational_age'] < 30]
        if len(f_0) != 0:
            # compute median of all features
            x = [np.median(f_0[k]) for k in feature_names]
            x = np.asarray(x)
            features[i_usr, 0, :] = x
            ttd.append(1000)

        # summarize features from week 30 binned every 1,2 or 7 days
        f_r = f[f['gestational_age'] >= 30]
        f_r['days_from_week_30'] = [((sw.date() - dd).days + 280.0) - 210 for sw, dd in
                                    zip(f_r['start_window'], users.loc[f_r['user_id'], 'due_date'])]

        for i in range(0, time_length):
            f_r_i = f_r[(f_r['days_from_week_30'] / bin_size).astype(int) == i]
            if (len(f_r_i) != 0):
                x = [np.median(f_r_i[k]) for k in feature_names]
                x = np.asarray(x)
                ttd.append(min(f_r_i['weeks_to_delivery'].values))
                features[i_usr, i + 1, :] = x
                # print(ttd)
        labels[i_usr] = min(ttd)

    return features, labels

# time_length = int(12 * 7 / cfg.time_binning['bin_size'])
#
# uids = list(set(features_orig['user_id']))
# features = np.zeros((len(uids), time_length + 1, len(cfg.features['names'])))
# labels = np.zeros((len(uids)))
#
# for i_usr, uid in enumerate(uids):
#     print('Processing user', i_usr, '/', len(uids))
#     ttd = []
#
#     # summarize features before week 30 to one timestep
#     f = features_orig[features_orig['user_id'] == uid]
#     f = f.sort_values(by=['start_window'])
#
#     # compute median from all features below 30 weeks
#     # f_0 = [a<30 for a in f['gestational_age']]
#     f_0 = f[f['gestational_age'] < 30]
#     if len(f_0) != 0:
#         # compute median of all features
#         x = [np.median(f_0[k]) for k in cfg.features['names']]
#         x = np.asarray(x)
#         features[i_usr, 0, :] = x
#         ttd.append(1000)
#
#     # summarize features from week 30 binned every 1,2 or 7 days
#     f_r = f[f['gestational_age'] >= 30]
#     f_r['days_from_week_30'] = [((sw.date() - dd).days + 280.0) - 210 for sw, dd in
#                                 zip(f_r['start_window'], users.loc[f_r['user_id'], 'due_date'])]
#
#     for i in range(0, time_length):
#         f_r_i = f_r[(f_r['days_from_week_30'] / cfg.time_binning['bin_size']).astype(int) == i]
#         if (len(f_r_i) != 0):
#             x = [np.median(f_r_i[k]) for k in cfg.features['names']]
#             x = np.asarray(x)
#             ttd.append(min(f_r_i['weeks_to_delivery'].values))
#             features[i_usr, i + 1, :] = x
#             # print(ttd)
#     labels[i_usr] = min(ttd)

#version without labor definition

import numpy as np
import pandas as pd
from . import regularity


# users = pd.read_pickle('./../data/04/users-04.pkl')
# recordings = pd.read_pickle('./../data/04/recordings-04.pkl')
# contractions = pd.read_pickle('./../data/04/contractions-04.pkl')
# aucs = pd.read_pickle('./../data/auc.pkl')



def extract(recordings, contractions, users, aucs, window_size, corner=False):

    features = pd.DataFrame(columns=['count', 'duration_median', 'duration_iqr', 'duration_rmssd',
                                     'auc_median', 'auc_iqr', 'auc_rmssd', 'variability', 'regularity_0',
                                     'regularity_1', 'regularity_2', 'regularity_e', 'rmssd', 'session_id', 'user_id',
                                     'start_window'])


    # for each recording in recordings
    # for each window between start rec and end rec
    #  look how many contractions in list of contractions

    total = len(recordings)
    rec_c = 0
    for index, rowR in recordings.iterrows():
        print(rec_c, '/', total)
        num_wins = int(np.floor(rowR['duration'].total_seconds() / window_size))
        cons_oneRec = contractions[contractions['session_id'] == index] #contractions for one recording
        aucs_rec = aucs[aucs['session_id']==index]['auc'][0]

        start_idx = 0 #index count to get AUCs from a window
        for win in range(0, num_wins):
            count = 0
            durations = []
            peaks = []
            start_window = rowR['created_at'] + pd.Timedelta(window_size * win, 's')
            for _, rowC in cons_oneRec.iterrows():
                peak = rowC['started_at'] + pd.Timedelta(int(rowC['delta_onset_to_peak']), 's')
                if (peak - start_window < pd.Timedelta(window_size, 's') and peak - start_window >= pd.Timedelta(0, 's')):
                    count = count + 1
                    durations.append(rowC['duration'].total_seconds())
                    # create subset with peak time
                    peaks.append(peak)
            peaks.sort()
            peak_diffs = np.abs([(t - s).total_seconds() for s, t in zip(peaks, peaks[1:])]) #don't need abs because time diffs always pos

            #get aucs for window
            aucs_win = aucs_rec[start_idx:start_idx+count]
            start_idx = start_idx+count


            #TODO edge values
            if(count==0):
                duration_median = [35 if corner else None][0]  # 1
                duration_iqr = [2*115 if corner else None][0]  # 2
                duration_rmssd = [3*115 if corner else None][0]  # 3

                # variability features
                variability = [3*(window_size-40) if corner else None][0]   # 3
                rmssd = [4 * (window_size - 120) if corner else None][0]   # 4
                r = [[2*1,2*0,2*0,2*1] if corner else [None, None, None, None]]  # 2
                r_0 = r[0][0]; r_1 = r[0][1]; r_2 = r[0][2]; r_e = r[0][3]


                # AUC features
                auc_median = [0 if corner else None][0]    # 1
                auc_iqr = [2*window_size if corner else None][0]   # 2
                auc_rmssd = [3*window_size if corner else None][0]   # 3



            elif(count == 1):

                # duration features
                duration_median = np.quantile(durations, 0.5)  # 1
                duration_iqr = [35 if corner else None][0]   # 2
                duration_rmssd = [2*115 if corner else None][0]   # 3

                # variability features
                variability = [2*(window_size-40) if corner else None][0]   # 3
                rmssd = [3*(window_size-120) if corner else None][0]   # 4
                r = [[1, 0, 0, 1] if corner else [None, None, None, None]]  # 2
                r_0 = r[0][0]; r_1 = r[0][1]; r_2 = r[0][2]; r_e = r[0][3]

                # AUC features
                auc_median = np.quantile(aucs_win, 0.5)  # 1
                auc_iqr = [window_size if corner else None][0]   # 2
                auc_rmssd = [2*window_size if corner else None][0]   # 3


            elif(count ==2):
                # duration features
                duration_median = np.quantile(durations, 0.5)  # 1
                duration_iqr = np.quantile(durations, 0.75) - np.quantile(durations, 0.25)  # 2
                duration_rmssd = [115 if corner else None][0]   # 3

                # variability features
                variability = [window_size-40 if corner else None][0]   # 3
                r_0, r_1, r_2, r_e = regularity.get(peaks, peak_diffs, window_size, start_window)  # 2
                rmssd = [2*(window_size-120) if corner else None][0]  # 4

                # AUC features
                auc_median = np.quantile(aucs_win, 0.5)  # 1
                auc_iqr = np.quantile(aucs_win, 0.75) - np.quantile(aucs_win, 0.25)  # 2
                auc_rmssd = [window_size-120 if corner else None][0]   # 3
            elif(count==3):
                # duration features
                duration_median = np.quantile(durations, 0.5)  # 1
                duration_iqr = np.quantile(durations, 0.75) - np.quantile(durations, 0.25)  # 2
                duration_rmssd = np.sqrt(np.mean([np.square(t - s) for s, t in zip(durations, durations[1:])]))  # 3

                # variability features
                variability = np.quantile(peak_diffs, 0.75) - np.quantile(peak_diffs, 0.25)  # 3
                r_0, r_1, r_2, r_e = regularity.get(peaks, peak_diffs, window_size, start_window)  # 2
                rmssd = [window_size-120 if corner else None][0]   # 4

                # AUC features
                auc_median = np.quantile(aucs_win, 0.5)  # 1
                auc_iqr = np.quantile(aucs_win, 0.75) - np.quantile(aucs_win, 0.25)  # 2
                auc_rmssd = np.sqrt(np.mean([np.square(t - s) for s, t in zip(aucs_win, aucs_win[1:])]))  # 3
            else:
                #duration features
                duration_median = np.quantile(durations, 0.5) #1
                duration_iqr = np.quantile(durations, 0.75) - np.quantile(durations, 0.25) #2
                duration_rmssd = np.sqrt(np.mean([np.square(t-s) for s,t in zip(durations, durations[1:])])) #3

                #variability features
                variability = np.quantile(peak_diffs, 0.75) - np.quantile(peak_diffs, 0.25) #3
                r_0, r_1, r_2, r_e = regularity.get(peaks, peak_diffs, window_size, start_window) #2
                rmssd = np.sqrt(np.mean([np.square(t - s) for s, t in zip(peak_diffs, peak_diffs[1:])])) #4


                #AUC features
                auc_median = np.quantile(aucs_win, 0.5) #1
                auc_iqr = np.quantile(aucs_win, 0.75) - np.quantile(aucs_win, 0.25) #2
                auc_rmssd = np.sqrt(np.mean([np.square(t-s) for s,t in zip(aucs_win, aucs_win[1:])])) #3



            features = features.append(
                {'count': count, 'duration_median': duration_median, 'duration_iqr': duration_iqr,
                 'duration_rmssd': duration_rmssd, 'variability': variability, 'regularity_0': r_0,
                 'regularity_1': r_1,
                 'regularity_2': r_2, 'regularity_e': r_e, 'rmssd': rmssd, 'auc_median': auc_median,
                 'auc_iqr': auc_iqr,
                 'auc_rmssd': auc_rmssd, 'session_id': rowR['id'], 'user_id': rowR['user_id'],
                 'start_window': start_window}, ignore_index=True)

        rec_c = rec_c+1
    features.sort_values('start_window')
    features = features.reset_index(drop=True)
    features['gestational_age'] = [((sw.date() - dd).days + 280.0) / 7.0 for sw, dd in zip(features['start_window'],
                                                             users.loc[recordings.loc[features['session_id'], 'user_id'], 'due_date'])]
    return features


#extract(recordings, contractions, aucs, 3600)
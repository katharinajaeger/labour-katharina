#compute area under the curve for all contractions of one recording

import numpy as np

def get(contractions, iup):
    aucs = []

    for ctx_idx in range(len(contractions)):
        signal = iup['signal'].tolist()[0]
        idx_start = int(contractions.iloc[ctx_idx]['delta_sart_to_onset'])
        idx_end = int(contractions.iloc[ctx_idx]['delta_sart_to_onset']) + int(
            contractions.iloc[ctx_idx]['delta_onset_to_peak']) + int(
            contractions.iloc[ctx_idx]['delta_peak_to_offset'])
        ctx = signal[idx_start:idx_end]
        baseline = (np.sum(signal[idx_start - 3:idx_start + 1]) + np.sum(signal[idx_end:idx_end + 4])) / 8
        ctx_bl = ctx - baseline
        auc = np.sum(ctx_bl)
        aucs.append(auc)
    return aucs
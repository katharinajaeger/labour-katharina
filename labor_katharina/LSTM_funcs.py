import numpy as np
import random



#same as rearrange_features2 but with synthesizing data randomly
def rearrange_features3(features_orig, feature_names, time_length, synthesize):
    uids = list(set(features_orig['user_id']))
    random.shuffle(uids)

    features = np.zeros((len(uids), time_length, len(feature_names)))
    labels = np.zeros((len(uids),))
    features_syn = np.zeros((len(uids), time_length, len(feature_names)))
    labels_syn = np.zeros((len(uids),))

    for i_usr, uid in enumerate(uids):
        print('Processing user', i_usr, '/', len(uids) - 1)
        f = features_orig[features_orig['user_id'] == uid]
        f = f.sort_values(by=['start_window'], ascending=False)
        f = f.reset_index(drop=True)

        date = f.iloc[0]['start_window'].date()
        cache = np.zeros((len(feature_names),))
        ttd = -np.ones((time_length,))

        c_feats = 0
        c_days = 0
        labels[i_usr] = f.iloc[0]['weeks_to_delivery']
        s_id = f.iloc[0]['session_id']

        ttd[time_length - c_days - 1] = f.iloc[0]['weeks_to_delivery']

        for i in range(len(f)):
            if c_days == time_length:
                break

            if (date == f.iloc[i]['start_window'].date()):
                cache = cache + f.iloc[i][feature_names].values
                c_feats = c_feats + 1
            else:
                # check session_id
                if (f.iloc[i]['session_id'] == s_id):
                    cache = cache + f.iloc[i][feature_names].values
                    c_feats = c_feats + 1
                else:
                    # store previous day
                    cache = cache / c_feats
                    features[i_usr, time_length - c_days - 1] = cache

                    # continue with next day
                    c_days = c_days + 1
                    cache = f.iloc[i][feature_names].values
                    c_feats = 1
                    date = f.iloc[i]['start_window'].date()
                    s_id = f.iloc[i]['session_id']

                    if c_days < time_length:
                        ttd[time_length - c_days - 1] = f.iloc[i]['weeks_to_delivery']

        # store last day
        cache = cache / c_feats
        features[i_usr, time_length - c_days - 1] = cache

        # synthesize data
        # check if still need to synthesize (num already synth vs frac*n)
        # random split between index: first entry != -1 and last entry != 0
        # take features[i_usr] fill with zeros to end from split index
        # assign label according to ttd[split index]
        # append to features and labels
        if synthesize:
            ind0 = next(x for x, val in enumerate(ttd) if val > -1)
            split_i = random.randrange(ind0, time_length) + 1
            f_synth = np.zeros(features[i_usr].shape)
            tmp = features[i_usr]
            l = split_i - ind0
            f_synth[time_length - l - ind0:time_length] = tmp[0:split_i]

            features_syn[i_usr] = f_synth
            labels_syn[i_usr] = min(ttd[ind0:split_i])

    if synthesize:
        return features_syn, labels_syn
    else:
        return features, labels

#summarizing features for every day and stringing them together with a max length
def rearrange_features2(features_orig, feature_names, time_length):
    uids = list(set(features_orig['user_id']))
    random.shuffle(uids)
    features = np.zeros((len(uids), time_length, len(feature_names)))
    labels = np.zeros((len(uids)))

    for i_usr, uid in enumerate(uids):
        print('Processing user', i_usr, '/', len(uids) - 1)
        f = features_orig[features_orig['user_id'] == uid]
        f = f.sort_values(by=['start_window'], ascending=False)
        f = f.reset_index(drop=True)

        date = f.iloc[0]['start_window'].date()
        cache = np.zeros((len(feature_names),))

        c_feats = 0
        c_days = 0
        labels[i_usr] = f.iloc[0]['weeks_to_delivery']
        s_id = f.iloc[0]['session_id']

        for i in range(len(f)):
            if c_days == time_length:
                break

            if (date == f.iloc[i]['start_window'].date()):
                cache = cache + f.iloc[i][feature_names].values
                c_feats = c_feats + 1
            else:
                # check session_id
                if (f.iloc[i]['session_id'] == s_id):
                    cache = cache + f.iloc[i][feature_names].values
                    c_feats = c_feats + 1
                else:
                    # store previous day
                    cache = cache / c_feats
                    features[i_usr, time_length - c_days - 1] = cache

                    # continue with next day
                    c_days = c_days + 1
                    cache = f.iloc[i][feature_names].values
                    c_feats = 1
                    date = f.iloc[i]['start_window'].date()
                    s_id = f.iloc[i]['session_id']


    return features, labels


#stringing together all available feature vectors per subject ordered by time + adding time delta to features
def rearrange_features1(features_orig, feature_names, time_length):
    uids = list(set(features_orig['user_id']))
    features = np.zeros((len(uids), time_length, len(feature_names) + 1))
    labels = np.zeros((len(uids)))

    f_names = feature_names.copy()
    f_names.append('time_delta')

    for i_usr, uid in enumerate(uids):
        print('Processing user', i_usr, '/', len(uids) - 1)
        f = features_orig[features_orig['user_id'] == uid]
        f = f.sort_values(by=['start_window'])
        f = f.reset_index(drop=True)
        time_delta = [int((f.iloc[i + 1]['start_window'] - f.iloc[i]['start_window']).total_seconds()) for i in
                      range(0, len(f) - 1)]
        time_delta.insert(0, 0)
        f['time_delta'] = time_delta

        f_x = f[f_names]
        f_y = int(f.iloc[-1]['weeks_to_delivery'])
        features[i_usr, :f_x.shape[0]] = f_x
        labels[i_usr] = f_y
    return features, labels



def bin_features(features_orig, users, bin_size, feature_names):
    time_length = int(12 * 7 / bin_size)

    uids = list(set(features_orig['user_id']))
    features = np.zeros((len(uids), time_length + 1, len(feature_names)))
    labels = np.zeros((len(uids)))

    for i_usr, uid in enumerate(uids):
        print('Processing user', i_usr, '/', len(uids)-1)
        ttd = []

        # summarize features before week 30 to one timestep
        f = features_orig[features_orig['user_id'] == uid]
        f = f.sort_values(by=['start_window'])

        # compute median from all features below 30 weeks
        # f_0 = [a<30 for a in f['gestational_age']]
        f_0 = f[f['gestational_age'] < 30]
        if len(f_0) != 0:
            # compute median of all features
            x = [np.median(f_0[k]) for k in feature_names]
            x = np.asarray(x)
            features[i_usr, 0, :] = x
            ttd.append(1000)

        # summarize features from week 30 binned every 1,2 or 7 days
        f_r = f[f['gestational_age'] >= 30]
        f_r['days_from_week_30'] = [((sw.date() - dd).days + 280.0) - 210 for sw, dd in
                                    zip(f_r['start_window'], users.loc[f_r['user_id'], 'due_date'])]

        for i in range(0, time_length):
            f_r_i = f_r[(f_r['days_from_week_30'] / bin_size).astype(int) == i]
            if (len(f_r_i) != 0):
                x = [np.median(f_r_i[k]) for k in feature_names]
                x = np.asarray(x)
                ttd.append(min(f_r_i['weeks_to_delivery'].values))
                features[i_usr, i + 1, :] = x
                # print(ttd)
        labels[i_usr] = min(ttd)

    return features, labels
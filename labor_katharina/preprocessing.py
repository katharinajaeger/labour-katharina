# function filters recordings according to percentage of sad segments
# updates list of users and contractions
import numpy as np
import pandas as pd

def filter_recordings(users, recordings, contractions, ss_percentage, threshold):
    IDs_q = ss_percentage[ss_percentage['fract'] <= threshold]
    recordings_q = recordings[recordings['id'].isin(IDs_q['session_id'])]

    if 'labor' in recordings.columns:
        # delete users that have no labor recording
        users_labor = set(recordings_q[recordings_q['labor'] == 1]['user_id'])
        recordings_q = recordings_q[recordings_q['user_id'].isin(users_labor)]

        # update contractions
        contractions_q = contractions[contractions['session_id'].isin(recordings_q['id'])]
        users_q = users[users['user_id'].isin(recordings_q['user_id'])]

        return users_q, recordings_q, contractions_q

    else:
        return users, recordings_q, contractions
import numpy as np
import pandas as pd

#fit regression to contraction features and return slope
def fit(features, feature_names):
    users = features['user_id'].unique()
    tmp_feature_names = list(feature_names)
    tmp_feature_names.append('user_id')
    trends = pd.DataFrame(columns=tmp_feature_names)
    trends = trends.set_index(['user_id'])

    # fit regression to features
    i=0
    for _, user in enumerate(users):
        # print(user)
        f = features[features['user_id'] == user]
        f = f.sort_values(['start_window'])
        t = {}
        t['user_id'] = user

        for feature in feature_names:
            y = f[feature].dropna().values.astype(float)
            if(len(y)>1):
                m = np.polyfit(y=y, x=np.arange(len(y)), deg=1)
                t[feature] = m[0]
            else:
                t[feature] = None
        trends = pd.concat([trends, pd.DataFrame(t, index=['user_id'])], sort=True)
    return trends

# features = pd.read_pickle('./../../data/04/features-04.pkl')
# feature_names = ['count', 'duration_median', 'duration_iqr', 'duration_rmssd',
#                                      'auc_median', 'auc_iqr', 'auc_rmssd', 'variability', 'regularity_0', 'regularity_1',
#                                      'regularity_2' , 'regularity_e', 'rmssd']
#
# fit(features, feature_names)
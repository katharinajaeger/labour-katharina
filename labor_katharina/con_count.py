import numpy as np
import pandas as pd

import numpy as np
import pandas as pd


# users = pd.read_pickle('./../data/users.pkl')
# recordings = pd.read_pickle('./../data/recordings.pkl')
# contractions = pd.read_pickle('./../data/contractions.pkl')
# recordings = recordings[0:10]


def extract(recordings, contractions, window_size, corner=False):
    countDF = pd.DataFrame(columns=['count'])

    # for each recording in recordings
    # for each window between start rec and end rec
    #  look how many contractions in list of contractions
    i=0
    total = len(recordings)
    for index, rowR in recordings.iterrows():
        print(i, '/', total)
        num_wins = int(np.floor(rowR['duration'].total_seconds() / window_size))
        cons = contractions[contractions['session_id'] == index]
        for win in range(0, num_wins):
            count = 0
            durations = []
            peaks = []
            start_window = rowR['created_at'] + pd.Timedelta(window_size * win, 's')
            for _, rowC in cons.iterrows():
                peak = rowC['started_at'] + pd.Timedelta(int(rowC['delta_onset_to_peak']), 's')
                if (peak - start_window < pd.Timedelta(window_size, 's') and peak - start_window >= pd.Timedelta(0, 's')):
                    count = count + 1
            countDF = countDF.append({'count': count, 'session_id': rowR['id'], 'labor': rowR['labor'], 'start_window': start_window}, ignore_index=True)
        i=i+1
    return countDF

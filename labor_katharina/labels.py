import numpy as np
import random


#creates time to delivery labels depending on number of classes
def TTD_create(features, C):
    features['label'] = -1
    for c in range(C - 1):
        features['label'] = [c if a == c else b for a, b in zip(features['weeks_to_delivery'], features['label'])]

    features['label'] = [C - 1 if a >= C-1 else b for a, b in zip(features['weeks_to_delivery'], features['label'])]

    y = features['label'].astype('int')
    return np.asarray(y), features

def TTD_create_2(features, C):
    features['label'] = -1

    for c in range(0, (C-1)*2, 2):
        features['label'] = [c/2 if a == c  or a == c+1 else b for a, b in zip(features['weeks_to_delivery'], features['label'])]

    features['label'] = [(C-1) if a >= (C-1)*2 else b for a, b in
                         zip(features['weeks_to_delivery'], features['label'])]

    y = features['label'].astype('int')
    return np.asarray(y), features

def TTD_create_LSTM(label, C):
    y = [-1 for i in label]

    for c in range(0, C):
        y = [c if a == c else b for a, b in zip(label, y)]
    y = [(C - 1) if a >= C - 1 else b for a, b in
         zip(label, y)]

    y = np.asarray(list(map(int, y)))
    return y

def TTD_create_LSTM_2(labels, C):
    y = [-1 for i in labels]

    for c in range(0, (C-1)*2, 2):
        y = [c/2 if a == c  or a == c+1 else b for a, b in zip(labels, y)]

    y = [(C-1) if a >= (C-1)*2 else b for a, b in
                         zip(labels, y)]

    y = np.asarray(list(map(int, y)))
    return y


def TTD_create_LSTM_time_dist(label,C):
    for i in range(label.shape[0]):
        for j in range(label.shape[1]):
            if label[i, j][0] >= C - 1:
                label[i, j][0] = C - 1
    return label
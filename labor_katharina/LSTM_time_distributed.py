import numpy as np
import random


def rearrange_features(features_orig, feature_names, time_length, synthesize):
    uids = list(set(features_orig['user_id']))
    random.shuffle(uids)

    features = np.zeros((len(uids), time_length, len(feature_names)))
    labels = np.zeros((len(uids), time_length, 1))

    for i_usr, uid in enumerate(uids):
        print('Processing user', i_usr, '/', len(uids) - 1)
        f = features_orig[features_orig['user_id'] == uid]
        f = f.sort_values(by=['start_window'], ascending=False)
        f = f.reset_index(drop=True)

        date = f.iloc[0]['start_window'].date()
        cache = np.zeros((len(feature_names),))
        ttd = -np.ones((time_length, 1))

        c_feats = 0
        c_days = 0
        s_id = f.iloc[0]['session_id']

        ttd[time_length - c_days - 1] = f.iloc[0]['weeks_to_delivery']

        for i in range(len(f)):
            if c_days == time_length:
                break

            if (date == f.iloc[i]['start_window'].date()):
                cache = cache + f.iloc[i][feature_names].values
                c_feats = c_feats + 1
            else:
                # check session_id
                if (f.iloc[i]['session_id'] == s_id):
                    cache = cache + f.iloc[i][feature_names].values
                    c_feats = c_feats + 1
                else:
                    # store previous day
                    cache = cache / c_feats
                    features[i_usr, time_length - c_days - 1] = cache

                    # continue with next day
                    c_days = c_days + 1
                    cache = f.iloc[i][feature_names].values
                    c_feats = 1
                    date = f.iloc[i]['start_window'].date()
                    s_id = f.iloc[i]['session_id']

                    if c_days < time_length:
                        ttd[time_length - c_days - 1] = f.iloc[i]['weeks_to_delivery']

        # store last day
        cache = cache / c_feats
        features[i_usr, time_length - c_days - 1] = cache
        labels[i_usr, :] = ttd

    return features, labels
###############definitions###########
data_paths_labor = {'train': {'users': './data/data/train/labor/users.pkl',
                         'recordings': './data/data/train/labor/recordings.pkl',
                             'contractions': './data/data/train/labor/contractions.pkl',
                         'auc': './data/data/train/labor/auc.pkl'},
             'test': {'users': './data/data/test/labor/users.pkl',
                         'recordings': './data/data/test/labor/recordings.pkl',
                             'contractions': './data/data/test/labor/contractions.pkl',
                         'auc': './data/data/test/labor/auc.pkl'}}

data_paths_TTD = {'train': {'users': './data/data/train/TTD/users.pkl',
                         'recordings': './data/data/train/TTD/recordings.pkl',
                             'contractions': './data/data/train/TTD/contractions.pkl',
                         'auc': './data/data/train/TTD/auc.pkl'},
             'test': {'users': './data/data/test/TTD/users.pkl',
                         'recordings': './data/data/test/TTD/recordings.pkl',
                             'contractions': './data/data/test/TTD/contractions.pkl',
                         'auc': './data/data/test/TTD/auc.pkl'}} 

run_train = 'train'

run_test = 'test'

label_labor = 'labor'

label_TTD = 'TTD'

##################config#############

run = run_train 

label = label_TTD

data_paths = data_paths_TTD

preprocessing = {'ss_percentage_path': './data/data/ss_percentage.pkl',
                'quality_threshold': 10}

feature_extraction = {'window_size': 3600,
                     'store': True,
                      'function_version': 4 #contraction_features_04.py; 1: contraction_features.py
                     }

postprocessing = {'remove_low_coverage_week': False,
                 'min': 30,
                 'max': 40}
